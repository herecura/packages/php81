pkgbase = php81
	pkgver = 8.1.31
	pkgrel = 1
	url = https://www.php.net/
	arch = x86_64
	license = PHP
	checkdepends = procps-ng
	makedepends = phpxx-common-dev
	makedepends = apache
	makedepends = aspell
	makedepends = db
	makedepends = gdbm
	makedepends = enchant
	makedepends = gd
	makedepends = gmp
	makedepends = icu
	makedepends = libsodium
	makedepends = libxslt
	makedepends = libzip
	makedepends = net-snmp
	makedepends = postgresql-libs
	makedepends = sqlite
	makedepends = systemd
	makedepends = tidy
	makedepends = unixodbc
	makedepends = curl
	makedepends = libtool
	makedepends = postfix
	makedepends = freetds
	makedepends = pcre2
	makedepends = libnsl
	makedepends = oniguruma
	source = https://php.net/distributions/php-8.1.31.tar.xz
	source = https://php.net/distributions/php-8.1.31.tar.xz.asc
	source = 0001-ext-intl-level-up-c-runtime-std-for-icu-74-and-onwar.patch
	source = apache.patch
	source = apache.conf
	source = php-fpm.patch
	source = php-fpm.tmpfiles
	source = php.ini.patch
	validpgpkeys = 528995BFEDFBA7191D46839EF9BA0ADA31CBD89E
	validpgpkeys = 39B641343D8C104B2B146DC3F9C39DC0B9698544
	validpgpkeys = F1F692238FBC1666E5A5CCD4199F9DFEF6FFBAFD
	sha256sums = c4f244d46ba51c72f7d13d4f66ce6a9e9a8d6b669c51be35e01765ba58e7afca
	sha256sums = SKIP
	sha256sums = 4c641108591a213fa91be915acf65f69e19beee3c8d29c2b285657f743e27f52
	sha256sums = 702b163c66c65af92dcad8d79f41bda84bcd5d863235fcf1497c33a86db9e4ca
	sha256sums = c799ed75a64865bfba81d987214b6bc7bfd1567ba69950649020e402048ac56b
	sha256sums = 50f4f4e08d574407a70f2977dd39118c48ee523d9625a2b925977f531d58125f
	sha256sums = eb0c0aec5b5fb282df0bfefd4d5bbc229ee80a5c5da6760ecf52697a23cc7175
	sha256sums = 1843995d37292ca51812a97e02f92037b8d47e96ba76b580670718a9e4028d56

pkgname = php81
	pkgdesc = A general-purpose scripting language that is especially suited to web development
	depends = phpxx-common
	depends = libxml2
	depends = curl
	depends = libzip
	depends = pcre2
	depends = argon2
	depends = oniguruma
	provides = php=8.1.31
	backup = etc/php81/php.ini

pkgname = php81-dev
	pkgdesc = dev files for PHP, needed for extension building
	depends = php81
	depends = phpxx-common-dev

pkgname = php81-cgi
	pkgdesc = CGI and FCGI SAPI for PHP
	depends = php81
	provides = php-cgi=8.1.31

pkgname = php81-apache
	pkgdesc = Apache SAPI for PHP
	depends = php81
	depends = apache
	depends = libnsl
	provides = php-apache=8.1.31
	backup = etc/httpd/conf/extra/php81_module.conf

pkgname = php81-fpm
	pkgdesc = FastCGI Process Manager for PHP
	depends = php81
	depends = systemd
	provides = php-fpm=8.1.31
	options = !emptydirs
	backup = etc/php81/php-fpm.conf
	backup = etc/php81/php-fpm.d/www.conf

pkgname = php81-embed
	pkgdesc = Embedded PHP SAPI library
	depends = php81
	depends = systemd-libs
	depends = libnsl
	depends = libxcrypt
	provides = php-embed=8.1.31
	options = !emptydirs

pkgname = php81-phpdbg
	pkgdesc = Interactive PHP debugger
	depends = php81
	provides = php-phpdbg=8.1.31
	options = !emptydirs

pkgname = php81-bcmath
	pkgdesc = BCMath Arbitrary Precision Mathematics module for PHP
	depends = php81
	provides = php-bcmath=8.1.31

pkgname = php81-bz2
	pkgdesc = Bzip2 module for PHP
	depends = php81
	provides = php-bz2=8.1.31

pkgname = php81-calendar
	pkgdesc = Calendar module for PHP
	depends = php81
	provides = php-calendar=8.1.31

pkgname = php81-dba
	pkgdesc = Database (dbm-style) Abstraction Layer module for PHP
	depends = php81
	depends = db
	depends = gdbm
	provides = php-dba=8.1.31

pkgname = php81-dblib
	pkgdesc = dblib module for PHP
	depends = php81
	depends = freetds
	provides = php-dblib=8.1.31

pkgname = php81-enchant
	pkgdesc = enchant module for PHP
	depends = php81
	depends = enchant
	provides = php-enchant=8.1.31

pkgname = php81-exif
	pkgdesc = Exchangeable image information module for PHP
	depends = php81
	provides = php-exif=8.1.31

pkgname = php81-ffi
	pkgdesc = Foreign Function Interface module for PHP
	depends = php81
	provides = php-ffi=8.1.31

pkgname = php81-ftp
	pkgdesc = FTP module for PHP
	depends = php81
	provides = php-ftp=8.1.31

pkgname = php81-gettext
	pkgdesc = Gettext module for PHP
	depends = php81
	provides = php-gettext=8.1.31

pkgname = php81-gd
	pkgdesc = gd module for PHP
	depends = php81
	depends = gd
	provides = php-gd=8.1.31

pkgname = php81-gmp
	pkgdesc = GNU Multiple Precision module for PHP
	depends = php81
	provides = php-gmp=8.1.31

pkgname = php81-iconv
	pkgdesc = iconv module for PHP
	depends = php81
	provides = php-iconv=8.1.31

pkgname = php81-intl
	pkgdesc = intl module for PHP
	depends = php81
	depends = icu
	provides = php-intl=8.1.31

pkgname = php81-ldap
	pkgdesc = LDAP module for PHP
	depends = php81
	provides = php-ldap=8.1.31

pkgname = php81-mysql
	pkgdesc = MySQL modules for PHP
	depends = php81
	provides = php-mysql=8.1.31

pkgname = php81-shmop
	pkgdesc = Shared Memory module for PHP
	depends = php81
	provides = php-shmop=8.1.31

pkgname = php81-soap
	pkgdesc = Soap module for PHP
	depends = php81
	provides = php-soap=8.1.31

pkgname = php81-sockets
	pkgdesc = sockets module for PHP
	depends = php81
	provides = php-sockets=8.1.31

pkgname = php81-sodium
	pkgdesc = sodium module for PHP
	depends = php81
	depends = libsodium
	provides = php-sodium=8.1.31

pkgname = php81-sysvipc
	pkgdesc = Sys V IPC modules for PHP
	depends = php81
	provides = php-sysvipc=8.1.31

pkgname = php81-odbc
	pkgdesc = ODBC modules for PHP
	depends = php81
	depends = unixodbc
	provides = php-odbc=8.1.31

pkgname = php81-pgsql
	pkgdesc = PostgreSQL modules for PHP
	depends = php81
	depends = postgresql-libs
	provides = php-pgsql=8.1.31

pkgname = php81-pspell
	pkgdesc = pspell module for PHP
	depends = php81
	depends = aspell
	provides = php-pspell=8.1.31

pkgname = php81-snmp
	pkgdesc = snmp module for PHP
	depends = php81
	depends = net-snmp
	provides = php-snmp=8.1.31

pkgname = php81-sqlite
	pkgdesc = sqlite module for PHP
	depends = php81
	depends = sqlite
	provides = php-sqlite=8.1.31

pkgname = php81-tidy
	pkgdesc = tidy module for PHP
	depends = php81
	depends = tidy
	provides = php-tidy=8.1.31

pkgname = php81-xsl
	pkgdesc = xsl module for PHP
	depends = php81
	depends = libxslt
	provides = php-xsl=8.1.31
